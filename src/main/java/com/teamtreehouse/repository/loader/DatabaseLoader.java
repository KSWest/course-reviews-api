package com.teamtreehouse.repository.loader;

import com.teamtreehouse.entity.Course;
import com.teamtreehouse.entity.Review;
import com.teamtreehouse.entity.User;
import com.teamtreehouse.repository.CourseRepository;
import com.teamtreehouse.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@Component
public class DatabaseLoader implements ApplicationRunner {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Course course = new Course("Java Basics", "URL of Java basics course");
        course.addReview(new Review(3, "Some review"));
        courseRepository.save(course);

        String[] templates = {
                "Up and running with %s",
                "%s basics",
                "%s for beginners",
                "%s for neckbeards",
                "Under the hood: %s"
        };

        String[] buzzwords = {
                "Spring Data Rest",
                "Java 9",
                "Scala",
                "Groovy",
                "Hibernate",
                "Spring HATEOAS"
        };

        List<User> students = Arrays.asList(
                new User("Eric", "Cartman", "ecartman", "password", new String[]{"ROLE_USER"}),
                new User("Kenny", "McCormic", "kmccormic", "password", new String[]{"ROLE_USER"}),
                new User("Stan", "Marsh", "smarsh", "password", new String[]{"ROLE_USER"}),
                new User("Kyle", "Proflowski", "kproflowski", "password", new String[]{"ROLE_USER"}),
                new User("Batters", "Stotch", "bstotch", "password", new String[]{"ROLE_USER"}),
                new User("Chuck", "Norris", "cnorris", "password", new String[]{"ROLE_USER"}),
                new User("Homer", "Simpson", "hsimpson", "password", new String[]{"ROLE_USER"})
        );

        userRepository.save(students);

        List<Course> bunchOfCourses = new ArrayList<>();

        IntStream.range(0, 100)
                .forEach(i -> {
                    String template = templates[i % templates.length];
                    String buzzword = buzzwords[i % buzzwords.length];
                    String title = String.format(template, buzzword);
                    Course c = new Course(title, "www.example.com");
                    Review review = new Review((i % 5) + 1, String.format("Moar %s please!!!", buzzword));
                    review.setReviewer(students.get(i % students.size()));
                    c.addReview(review);
                    bunchOfCourses.add(c);
                });

        courseRepository.save(bunchOfCourses);
    }
}
